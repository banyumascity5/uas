import React from "react";
import { StyleSheet, Text, View,Animated,Image } from "react-native";

import Logo from '../Image/llk.png';

const App = () => (
  <View style={{
    position: 'absolute',
    top:0,
    bottom:0,
    left:0,
    right:0,
    backgroundColor: '#00ffff'
  }}>
    <Animated.View style={{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    }}>
        <Image source={Logo} style= {{
            width:120,
            height:120,
        }}></Image>
        <Text style={{
            fontSize:25,
            fontWeight: 'bold',
            color: 'black',
            alignContent:'center'
        }}>AYO MEMASAK</Text>
    </Animated.View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top:0,
    bottom:0,
    left:0,
    right:0,
    backgroundColor: 'BGColor',
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000c0"
  }
});

export default App;